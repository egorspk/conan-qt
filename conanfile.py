#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from conans import AutoToolsBuildEnvironment, VisualStudioBuildEnvironment
from conans import ConanFile, tools
from conans.errors import ConanException
from distutils.spawn import find_executable


class QtConan(ConanFile):
    name = "Qt"
    version = "5.7.1"
    license = "http://doc.qt.io/qt-5/lgpl.html"
    homepage = "https://www.qt.io"
    url = "https://bitbucket.org/egorspk/conan-qt"
    description = "Conan.io package for Qt library. Use only for windows and visual studio"
    source_dir = "qt5"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "opengl": ["desktop", "dynamic"],
        "activeqt": [True, False],
        "canvas3d": [True, False],
        "connectivity": [True, False],
        "gamepad": [True, False],
        "graphicaleffects": [True, False],
        "imageformats": [True, False],
        "location": [True, False],
        "serialport": [True, False],
        "svg": [True, False],
        "tools": [True, False],
        "translations": [True, False],
        "webengine": [True, False],
        "websockets": [True, False],
        "xmlpatterns": [True, False],
        "openssl": ["no", "yes", "linked"]
    }
    default_options = "shared=True", "fPIC=True", "opengl=desktop", "activeqt=False", \
                      "canvas3d=False", "connectivity=False", "gamepad=False", \
                      "graphicaleffects=False", "imageformats=False", "location=False", \
                      "serialport=False", "svg=False", "tools=False", "translations=False", \
                      "webengine=False", "websockets=False", "xmlpatterns=False", "openssl=no"
    generators = "visual_studio"

    def requirements(self):
        if self.options.openssl == "yes" or self.options.openssl == "linked":
            self.requires("OpenSSL/1.0.2l@conan/stable")

    def source(self):
        submodules = ["qtbase"]

        if self.options.activeqt:
            submodules.append("qtactiveqt")
        if self.options.canvas3d:
            submodules.append("qtcanvas3d")
        if self.options.connectivity:
            submodules.append("qtconnectivity")
        if self.options.gamepad:
            submodules.append("qtgamepad")
        if self.options.graphicaleffects:
            submodules.append("qtgraphicaleffects")
        if self.options.imageformats:
            submodules.append("qtimageformats")
        if self.options.location:
            submodules.append("qtlocation")
        if self.options.serialport:
            submodules.append("qtserialport")
        if self.options.svg:
            submodules.append("qtsvg")
        if self.options.tools:
            submodules.append("qttools")
        if self.options.translations:
            submodules.append("qttranslations")
        if self.options.webengine:
            submodules.append("qtwebengine")
        if self.options.websockets:
            submodules.append("qtwebsockets")
        if self.options.xmlpatterns:
            submodules.append("qtxmlpatterns")

        self.run("git clone https://code.qt.io/qt/qt5.git")
        self.run("cd %s && git checkout %s" % (self.source_dir, self.version))
        self.run("cd %s && git submodule update --init %s" % (self.source_dir, " ".join(submodules)))

    def build(self):
        if self.settings.os != "Windows" or self.settings.compiler != "Visual Studio":
            raise ConanException("This recipe only for windows and visual studio")
        else:
            args = ["-opensource", "-confirm-license", "-nomake examples", "-nomake tests",
                    "-prefix %s" % self.package_folder]
            if not self.options.shared:
                args.insert(0, "-static")
            if self.settings.build_type == "Debug":
                args.append("-debug")
            else:
                args.append("-release")
            self._build_msvc(args)

    def _build_msvc(self, args):
        # get build tool
        build_command = find_executable("jom.exe")
        if build_command:
            build_args = ["-j", str(tools.cpu_count())]
            self.output.info("Using '%s' threads" % str(tools.cpu_count()))
        else:
            build_command = "nmake.exe"
            build_args = []
        self.output.info("Using '%s %s' to build" % (build_command, " ".join(build_args)))

        # check ActivePerl
        active_perl = find_executable("perl.exe")
        if active_perl is None:
            raise ConanException("Unable find ActivePerl in PATH")

        env = {}
        env.update({'PATH': ['%s/qtbase/bin' % self.source_folder,
                             '%s/gnuwin32/bin' % self.source_folder,
                             '%s/qtrepotools/bin' % self.source_folder]})

        env_build = VisualStudioBuildEnvironment(self)
        env.update(env_build.vars)

        with tools.environment_append(env):
            vcvars = tools.vcvars_command(self.settings)

            args += ["-opengl %s" % self.options.opengl]
            if self.options.openssl == "no":
                args += ["-no-openssl"]
            elif self.options.openssl == "yes":
                args += ["-openssl"]
            else:
                args += ["-openssl-linked"]

            self.output.info("### Configuration ###")
            self.run("%s && cd %s && configure.bat %s"
                     % (vcvars, self.source_dir, " ".join(args)))

            self.output.info("### Build ###")
            self.run("%s && cd %s && %s %s"
                     % (vcvars, self.source_dir, build_command, " ".join(build_args)))

            self.output.info("### Install ###")
            self.run("%s && cd %s && %s install" % (vcvars, self.source_dir, build_command))

    def package_info(self):
        libs = ['Concurrent', 'Core', 'DBus',
                'Gui', 'Network', 'OpenGL',
                'Sql', 'Test', 'Widgets', 'Xml']

        if self.settings.build_type == "Debug":
            self.cpp_info.libs = ['qtmaind']
        else:
            self.cpp_info.libs = ['qtmain']

        self.cpp_info.includedirs = ["include"]
        for lib in libs:
            if self.settings.build_type == "Debug":
                suffix = "d"
            else:
                suffix = ""
            self.cpp_info.libs += ["Qt5%s%s" % (lib, suffix)]
            self.cpp_info.includedirs += ["include/Qt%s" % lib]

        self.env_info.path.append(os.path.join(self.package_folder, "bin"))
